package edu.upenn.cis.cis455.crawler;

import edu.upenn.cis.cis455.crawler.spout.*;
import edu.upenn.cis.cis455.crawler.bolt.*;
import edu.upenn.cis.cis455.crawler.utils.BlockingQueue;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.tuple.Fields;

import org.apache.logging.log4j.Level;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Crawler implements CrawlMaster {
	
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.
	
    static final int    NUM_WORKERS    = 10;
	static final String URL_SPOUT      = "URL_SPOUT";
    static final String DOCUMENT_BOLT  = "DOCUMENT_BOLT";
    static final String EXTRACTOR_BOLT = "EXTRACTOR_BOLT";
    static final String PARSER_BOLT    = "PARSER_BOLT";
    static final String MATCHER_BOLT   = "MATCHER_BOLT";
    
    public static TopologyBuilder builder = new TopologyBuilder();
    
    public static Config config;
    
    public static LocalCluster cluster;
    
    public static StorageInterface db;
    
    public static BlockingQueue urlQueue;
    
    public static int size;
    
    public static int countMax;
    
    public int totalCorpse;
    
    public boolean notifyExit = false;
    
    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        // TODO: initialize
    	Crawler.urlQueue = new BlockingQueue();
    	Crawler.urlQueue.enqueue(startUrl);
    	Crawler.db = db;
    	Crawler.size = size;
    	Crawler.countMax = count;
    	
    	config = new Config(); 
    	
        CrawlerQueueSpout spout = new CrawlerQueueSpout();
        DocumentFetcherBolt fetcherBolt = new DocumentFetcherBolt();
        LinkExtractorBolt extractorBolt = new LinkExtractorBolt();
        DOMParserBolt parserBolt = new DOMParserBolt();
        PathMatcherBolt matcherBolt = new PathMatcherBolt();
        
        builder.setSpout(URL_SPOUT, spout, 1);
        
        builder.setBolt(DOCUMENT_BOLT, fetcherBolt, 3).fieldsGrouping(URL_SPOUT, new Fields("URL"));
        
        builder.setBolt(EXTRACTOR_BOLT, extractorBolt, 4).shuffleGrouping(DOCUMENT_BOLT);
        
        builder.setBolt(PARSER_BOLT, parserBolt, 4).shuffleGrouping(DOCUMENT_BOLT);
        
        builder.setBolt(MATCHER_BOLT, matcherBolt, 3).shuffleGrouping(PARSER_BOLT);
        
        cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
		try {
			String str = mapper.writeValueAsString(topo);

			System.out.println("The StormLite topology is:\n" + str);
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * Main thread
     */
    public void start() {
        cluster.submitTopology("test", config,
        		builder.createTopology());
    }

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
    	this.totalCorpse = db.getCorpusSize();
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
    	incCount();
        if(this.totalCorpse > countMax || urlQueue.getSize() == 0) {
        	return true;
        }
        return false;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    	
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
    	
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) throws Exception {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.INFO);
        
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;
        
        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
        
        Crawler crawler = new Crawler(startUrl, db, size, count);
        
        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();
        
        Thread.sleep(30000);
        while (!crawler.isDone()) {
        	try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }	
        }
        
        // TODO: final shutdown
        
        cluster.killTopology("test");
    	cluster.shutdown();
    	
    	Crawler.db.close();
    	
    	System.out.println("Done crawling!");
    }
}