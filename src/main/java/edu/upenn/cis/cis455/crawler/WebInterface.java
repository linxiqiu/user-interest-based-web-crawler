package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static spark.Spark.*;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.DocumentChannel;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegisterHandler;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;

import org.apache.logging.log4j.Level;

public class WebInterface {
    public static void main(String args[]) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.INFO);
    	
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
                System.out.println("yes");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        port(45555);
        StorageInterface database = StorageFactory.getDatabaseInstance(args[0]);

        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
        	if (!Files.exists(Paths.get(args[1]))) {
        		try {
                    Files.createDirectory(Paths.get(args[1]));
                    System.out.println("yes");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        	}
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }

        before("/*", "*/*", testIfLoggedIn);
        // TODO:  add /register, /logout, /index.html, /, /lookup
        get("/index.html", (req, res) -> {
        	return "Welcome " + req.session().attribute("user");
        });
        get("/logout", (req, res) -> {
        	req.session().invalidate();
        	req.session(false);
        	res.redirect("/login-form.html");

        	return "";
        });
        get("/shutdown", (req, res) -> {
        	database.close();
        	stop();
        	return "";
        });
        post("/register", new RegisterHandler(database));
        post("/login", new LoginHandler(database));
        get("/login", (req, res)->{
        	res.redirect("/");
        	return "";
        });
        
        get("/create/:name", (req, res) -> {
        	String channelName = req.params(":name");
        	String XPathPattern = req.queryParams("xpath");
        	String userName = req.session().attribute("user");
        	System.out.println(XPathPattern);
        	if (channelName == null || XPathPattern == null)
        		halt(400);
        	
        	Channel channel = new Channel(XPathPattern, userName, channelName);
        	database.addChannel(channel);
        	System.out.println(database.getChannel().size());
        	return "";
        });
        
        get("/", (req, res) -> {
        	ArrayList<Channel> channels = database.getChannel();
        	
        	if (channels.size() == 0) 
        		return "this is home";
        	else {
        		String content = "<div class=\"channelheader\">";
        		for (Channel channel : channels) {
        			content += channel.toString();
        			content += "</div>\r\n";
        		}
        		res.body(content);
        		return res.body();
        	}
        });
        
        get("/show", (req, res) -> {
        	String channelName = req.queryParams("channel");
        	Channel channel = database.getChannel(channelName);
        	StringBuffer s = new StringBuffer();
        	for (DocumentChannel dc : channel.getDocs()) {
        		String content = "";
        		content += "Crawled on: " + dc.getCrawlTime() + "\r\n";
        		content += "Location: " + dc.getURL() + "\r\n";
        		content += "<div class=”document”>\r\n";
        		content += dc.getContent() + "\r\n";
        		content += "</div>\r\n";
        		s.append(content);
        	}
        	res.body(s.toString());
        	return res.body();
        });
        
        get("/lookup", (req, res) ->{
        	String url = req.queryParams("url");
        	URLInfo urlEncap = new URLInfo(url);
        	System.out.println(urlEncap.toString());
        	String content = database.getDocument(urlEncap.toString());
        	if (content == null)
        		halt(404, "Not Found!!!");
        	res.body(content);
        	return res.body();
        });
        System.out.println("Waiting for requests");
        
        awaitInitialization();
    }
}
