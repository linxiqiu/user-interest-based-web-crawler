package edu.upenn.cis.cis455.crawler.bolt;

import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DOMParserBolt implements IRichBolt{
	static Logger log = LogManager.getLogger(DOMParserBolt.class);
	
	Fields schema = new Fields("EventList");
	
	/**
     * To make it easier to debug: we have a unique ID for each
     * instance of the WordCounter, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
    
    /**
     * This is where we send our output stream
     */
    private OutputCollector collector;
    
    /*
     * Storing OccurrenceEvents generated in 1 document
     */
    LinkedList<OccurrenceEvent> list = new LinkedList<OccurrenceEvent>(); 
    
    private URLInfo urlEncap;
    private String url;
    private String content;
    private String contentType;
    
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(schema);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		list.clear();
	}

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		urlEncap = (URLInfo)input.getObjectByField("URL");
		url = urlEncap.toString();
		content = input.getStringByField("Content");
		contentType = input.getStringByField("ContentType");
		
		Document doc;
		if (contentType.equals("xml")){
			doc = Jsoup.parse(content, "", Parser.xmlParser());
		}
		else 
			doc = Jsoup.parse(content);
		
		Node root = doc.root();
		for (Node n : doc.childNodes()) {
			if (n.nodeName().toLowerCase().equals("rss") || n.nodeName().toLowerCase().equals("html")) {
				root = n;
			}
		}
		
		traverse(root);
		
		if(list.size() > 0)
			this.collector.emit(new Values<Object>(list));
	}

	public void traverse(Node node) {
		OccurrenceEvent occEve = new OccurrenceEvent("OPEN", node.nodeName());
		occEve.setURL(url);
		if (node.nodeName() != "#text")
			list.add(occEve);
			
		if (node.childNodes().size() == 0) {
			occEve = new OccurrenceEvent("TEXT", node.toString().trim());
			occEve.setURL(url);
			list.add(occEve);
		}
		
		for (Node n : node.childNodes()) {
			traverse(n);
		}
		occEve = new OccurrenceEvent("CLOSE", node.nodeName());
		occEve.setURL(url);
		if (node.nodeName() != "#text")
			list.add(occEve);
	}
	
	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return schema;
	}

}
