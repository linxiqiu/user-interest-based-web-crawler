package edu.upenn.cis.cis455.crawler.bolt;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.*;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DocumentFetcherBolt implements IRichBolt{
	static Logger log = LogManager.getLogger(DocumentFetcherBolt.class);
	
	Fields schema = new Fields("URL", "ContentType", "Content");
	
    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the WordCounter, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
    
    /**
     * This is where we send our output stream
     */
    private OutputCollector collector;
    
    private int crawlDelay = 0;
    
    public DocumentFetcherBolt() {
    }

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
	}
	
	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		String url = input.getStringByField("URL");
		this.crawlDelay = 0;
		// Crawling politeness
		URLInfo urlEncap = new URLInfo(url);
		try {
			if (robotsTXT(urlEncap) == true) {
				log.info(urlEncap.toString() + ": Blocked");
				return;
			}
			if (this.crawlDelay > 0)
				Thread.sleep(crawlDelay);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			tacklingConnection(urlEncap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(getExecutorId() + ": " + input.toString());
	}
    
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(schema);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		Crawler.db.close();
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return schema;
	}
	
	private boolean robotsTXT(URLInfo url) throws IOException {
		URL u = new URL((url.isSecure() ? "https://" : "http://") + url.getHostName() + "/robots.txt");
		String fp = url.getFilePath().endsWith("/") ? url.getFilePath() : url.getFilePath() + "/";
		
		HttpURLConnection con = (HttpURLConnection)u.openConnection();
		if(url.isSecure() == true)
			con = (HttpsURLConnection) con;
		con.setRequestMethod("HEAD");
		con.setRequestProperty("User-Agent", "cis455crawler");
		con.connect();
		
		if (con.getResponseCode() != 200) {
			con.disconnect();
			return false;
		}
		
		InputStream in = u.openStream();
		byte[] content = in.readAllBytes();
		String[] contentString = new String(content).split("\n");
		int position = -1;
		boolean isBlocked = false;
		
		for (int i = 0; i < contentString.length; i++) {
			if(contentString[i].toLowerCase().equals("user-agent: cis455crawler")) {
				position = i;
				break;
			}
		}
		if (position == -1) {
			for (int i = 0; i < contentString.length; i++) {
				if(contentString[i].toLowerCase().equals("user-agent: *")) {
					position = i;
					break;
				}
			}
		}
		if (position == -1)
			return false;
		
		
		while (contentString[position].length() != 0) {
			String[] rule = contentString[position].split(": ");
			if (rule[0].toLowerCase().equals("disallow")) {
				
				if(fp.startsWith(rule[1])) {
					isBlocked = true;
				}
			}
			else if (rule[0].toLowerCase().equals("crawl-delay")) {
				this.crawlDelay = Integer.valueOf(rule[1]);
			}
			position++;
		}
		return isBlocked;
	}
	
	private void tacklingConnection(URLInfo urlEncap) throws IOException {
		URL URLobj = new URL(urlEncap.toString());
		HttpURLConnection con = (HttpURLConnection)URLobj.openConnection();
		if (urlEncap.isSecure()) {
			con = (HttpsURLConnection)URLobj.openConnection();
		}
		con.setRequestMethod("HEAD");
		con.setRequestProperty("User-Agent", "cis455crawler");
		con.connect();
		
		int resCode = con.getResponseCode();
		double contentLength = con.getContentLength() / 1000000.0; 
		String contentType = con.getContentType().split(";")[0];
		
		if (resCode != 200) {
			log.info("Connection failed");
			con.disconnect();
			return;
		}
		
		if (contentLength > Crawler.size * 1000000.0) {
			log.info("Too large!");
			con.disconnect();
			return;
		}
			
		if (contentType.equals("text/html")) {
			log.info("indexing");
			InputStream inputStream = URLobj.openStream();
			byte[] contentByte = inputStream.readAllBytes();
			String content = new String(contentByte);
			if (content != null) {
				int status = Crawler.db.addDocument(urlEncap.toString(), content);
				if (status == 0) {
					log.info(urlEncap.toString() + ": Indexing failed: Content seen");
				}
				else if (status == -1) { // indexing success
					log.info(urlEncap.toString() + ": Indexing failed");
				}
				else {
					log.info(urlEncap.toString() + ": Indexing success");
				}

				this.collector.emit(new Values<Object>(urlEncap, new String("html"), content));
			}
			inputStream.close();
		}
		else if (contentType.contains("/xml")) {
			InputStream inputStream = URLobj.openStream();
			byte[] contentByte = inputStream.readAllBytes();
			String content = new String(contentByte);
			
			if (content != null) {
				int status = Crawler.db.addDocument(urlEncap.toString(), content);
				if (status == 0) {
					log.info(urlEncap.toString() + ": Indexing failed: Content seen");
				}
				else if (status == -1) { // indexing success
					log.info(urlEncap.toString() + ": Indexing failed");
				}
				else {
					log.info(urlEncap.toString() + ": Indexing success");
				}
			}
			
			collector.emit(new Values<Object>(urlEncap, new String("xml"), content));
			inputStream.close();
		}
		con.disconnect();
	}
}
