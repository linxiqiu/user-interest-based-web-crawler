package edu.upenn.cis.cis455.crawler.bolt;

import java.util.Map;
import java.util.UUID;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.*;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LinkExtractorBolt implements IRichBolt {
	static Logger log = LogManager.getLogger(LinkExtractorBolt.class);
	
	Fields myFields = new Fields();
	/**
     * To make it easier to debug: we have a unique ID for each
     * instance of the WordCounter, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
    
    /**
     * This is where we send our output stream
     */
    private OutputCollector collector;

    public LinkExtractorBolt() {
    }
    
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(myFields);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		Crawler.db.close();
	}
	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
	}

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		log.info("doing");
		
		if (input.getStringByField("ContentType").equals("xml"))
			return;
		
		String content = input.getStringByField("Content");
		URLInfo url = (URLInfo)input.getObjectByField("URL");
		log.info("Getting: " + url.toString());
		
		Document doc = Jsoup.parse(content);
		
        Elements links = doc.select("a[href]");
        for (Element link : links) {
        	URLInfo urlNew = new URLInfo(link.attr("href"));    
        	if (urlNew.getHostName() == null) {
        		if (link.attr("href").startsWith("//")) {
        			
        			String urlLegal = (url.isSecure() ? "https://" : "http://") + link.attr("href");
        			urlNew = new URLInfo(urlLegal);
        		}
        		else if (link.attr("href").startsWith("#")) {
        			urlNew.setPortNo(0);
        		}
        		else  {
        			urlNew.setHostName(url.getHostName());
            		urlNew.setPortNo(url.getPortNo());
            		urlNew.setSecure((url.getPortNo() == 443) ? true : false);
            		
            		String fp = "";
            		
            		if (url.getFilePath() == null) {
            			fp = "/" + link.attr("href");
            		}
            		else if (url.getFilePath().endsWith(".html")) {
            			String pathTrim = url.getFilePath().substring(0, url.getFilePath().lastIndexOf("/"));
            			fp = (!pathTrim.endsWith("/") && !link.attr("href").startsWith("/")) ?
                				pathTrim + "/" + link.attr("href") :
                					pathTrim + link.attr("href");
            		}
            		else {
            			fp = (!url.getFilePath().endsWith("/") && !link.attr("href").startsWith("/")) ?
                				url.getFilePath() + "/" + link.attr("href") :
                					url.getFilePath() + link.attr("href");
            		}
            			
            		
            		urlNew.setFilePath(fp);
        		}
        	}
        	if(urlNew.getPortNo() == 443 || urlNew.getPortNo() == 80) {
        		Crawler.urlQueue.enqueue(urlNew.toString());
        	}
        }
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// Do nothing
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return myFields;
	}

}
