package edu.upenn.cis.cis455.crawler.bolt;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.DocumentChannel;
import edu.upenn.cis.cis455.xpathengine.*;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class PathMatcherBolt implements IRichBolt {
	
	static Logger log = LogManager.getLogger(PathMatcherBolt.class);
	
	/**
	 * Occurrence Event
	 */
	private LinkedList<OccurrenceEvent> list = new LinkedList<OccurrenceEvent>();
	
	/**
	 * XPath Engine
	 */
	private XPathEngineImpl XPathEngine;
    
    /**
     * All registered channels are here, we need to update some of them if matched.
     */
    private ArrayList<Channel> channelList = new ArrayList<Channel>();
    
    /**
     * The state stack for each channel
     */
    private Map<Integer, List<Boolean>> stateList = new HashMap<Integer, List<Boolean>>();
    
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		list.clear();
		channelList.clear();
		stateList.clear();
	}

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		this.list = (LinkedList<OccurrenceEvent>) input.getObjectByField("EventList");
		System.out.println(list.size());
		this.XPathEngine = (XPathEngineImpl) XPathEngineFactory.getXPathEngine();
		
		ArrayList<Channel> channelList = Crawler.db.getChannel();
		
		int channelId = 1;
		
		boolean[] stateUpdate;
		
		for (Channel c : channelList) {
			String[] XPath = c.getChannel().substring(1).split("/");
			if (XPath[0].toLowerCase().equals("html")) {
				XPath = c.getChannel().toLowerCase().substring(1).split("/");
			}
			
			this.XPathEngine.setXPaths(XPath);
			stateList.put(channelId, new ArrayList<Boolean>());
			channelId++;
		}
		
		for (OccurrenceEvent e : list) {
			//System.out.println(e.getType() + ": " + e.getValue());
			stateUpdate = XPathEngine.evaluateEvent(e);
			int iter = 0;
			// Insert new state
			if (e.getType().equals(OccurrenceEvent.Type.Open)) {
				for (List<Boolean> sl : stateList.values()) {
					sl.add(stateUpdate[iter]);
					iter++;
				}
			}
			
			// Modify state
			else if (e.getType().equals(OccurrenceEvent.Type.Text)) {
				for (List<Boolean> sl : stateList.values()) {
					if (sl.get(sl.size() - 1) == true && stateUpdate[iter] == false) {
						sl.set(sl.size() - 1, stateUpdate[iter]);
					}
					
					// Check Populating
					Channel ch = channelList.get(iter);
					if (sl.size() == ch.getChannelNum() && !sl.contains(Boolean.FALSE)) {
						ch.addDocs(new DocumentChannel(e.getURL(), e.getValue()));
					}
					iter++;
				}
			}
			
			// Annihilate state 
			else {
				for (List<Boolean> sl : stateList.values()) {
					Channel ch = channelList.get(iter);
					
					// Update the channel
					if (sl.size() == ch.getChannelNum() && !sl.contains(Boolean.FALSE)) {
						Crawler.db.addChannel(ch);
					}
					
					sl.remove(sl.size() - 1);
					iter++;
				}
			}
			
		}
		/**
		 * When 1 document finish, clean up!
		 */
		cleanup();
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

}
