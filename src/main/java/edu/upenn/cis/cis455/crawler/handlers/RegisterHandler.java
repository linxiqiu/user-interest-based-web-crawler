package edu.upenn.cis.cis455.crawler.handlers;

import static spark.Spark.*;
import spark.Request;
import spark.Route;
import spark.Response;
import spark.Session;
import spark.HaltException;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class RegisterHandler implements Route{
    StorageInterface db;

    public RegisterHandler(StorageInterface db) {
        this.db = db;
    }

	@Override
	public Object handle(Request req, Response resp) throws HaltException {
		// TODO Auto-generated method stub
        String user = req.queryParams("username");
        String pass = req.queryParams("password");
		if (db.addUser(user, pass) == -1) {
			halt(409, "User Exist!");
		}
		else {
			Session session = req.session();
            session.maxInactiveInterval(300);
            session.attribute("user", user);
            session.attribute("password", pass);
            System.out.println(session.attributes());
            String body = "<li><a href=\"/\">Main Page</a></li> \r\n";
            body += "<li><a href=\"/login\">Log in</a></li> \r\n";
            resp.body(body);
            return resp.body();
		}
		return "";
	}

}
