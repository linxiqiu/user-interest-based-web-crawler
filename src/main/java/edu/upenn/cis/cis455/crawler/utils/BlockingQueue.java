package edu.upenn.cis.cis455.crawler.utils;

import java.util.*;

public class BlockingQueue
{
	private ArrayList<String> urlList;

	public BlockingQueue()
	{
		urlList = new ArrayList<String>();
	}

	public synchronized int getSize()
	{
		return urlList.size();
	}

	public void insert(String url)
	{
		
		urlList.add(url);
	}

	public synchronized void enqueue(String urlNew)
	{
		insert(urlNew);
	}	

	public synchronized String dequeue() throws InterruptedException
	{
		while(urlList.size() == 0)
			wait();
		if(urlList.size() > 0)
			notify();
		String url = urlList.get(0);
		urlList.remove(0);
		return url;
	}
}
