package edu.upenn.cis.cis455.crawler.utils;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

import edu.upenn.cis.cis455.storage.StorageInterface;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CrawlerWorker extends Thread implements Runnable {
	Logger logger = LogManager.getLogger(CrawlerWorker.class);
	
	private StorageInterface db;
	
	public BlockingQueue urlQueue;
	
	private int size;
	
	private int docCollected = 0;
	
	private int crwalDelay;
	
	private boolean isWorking = false;
	
	public CrawlerWorker(StorageInterface db, BlockingQueue urlQueue, int size) {
		this.db = db;
		this.urlQueue = urlQueue;
		this.size = size;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (isWorking) {

			/* Poll a URL in the queue */
			try {
				System.out.println(urlQueue.getSize());
				String url = urlQueue.dequeue();
				URLInfo urlEncap = new URLInfo(url);
				tacklingConnection(urlEncap);
				
				if (this.crwalDelay > 0) {
					Thread.sleep(crwalDelay * 1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}

	private void tacklingConnection(URLInfo urlEncap) throws IOException {
		this.crwalDelay = 0;
		if (robotsTXT(urlEncap) == true) {
			logger.info(urlEncap.toString() + ": Blocked");
			return;
		}
			
		
		if (urlEncap.isSecure() == true) {
			URL URLobj = new URL(urlEncap.toString());
			HttpsURLConnection con = (HttpsURLConnection)URLobj.openConnection();
			con.setRequestMethod("HEAD");
			con.setRequestProperty("User-Agent", "cis455crawler");
			con.connect();
			
			int resCode = con.getResponseCode();
			double contentLength = con.getContentLength() / 1000000.0; 
			String contentType = con.getContentType().split(";")[0];
			
			if (contentLength > this.size)
				return;
			
			if (resCode != 200) {
				logger.info("Connection failed");
				return;
			}
			
			if (contentType.equals("text/html")) {
				parseHTMLContent(urlEncap);
			}
			if (contentType.contains("/xml")) {
				parseXMLContent(urlEncap);
			}
			con.disconnect();
		}
		else {
			URL URLobj = new URL(urlEncap.toString());
			HttpURLConnection con = (HttpURLConnection)URLobj.openConnection();
			con.setRequestMethod("HEAD");
			con.setRequestProperty("User-Agent", "cis455crawler");
			con.connect();
			
			int resCode = con.getResponseCode();
			double contentLength = con.getContentLength() / 1000000.0; 
			String contentType = con.getContentType().split(";")[0];
			
			if (resCode == 200 && contentLength < this.size && contentType.equals("text/html")) {
				parseHTMLContent(urlEncap);
			}
			
			if (resCode == 200 && contentLength < this.size && contentType.contains("/xml")) {
				parseXMLContent(urlEncap);
			}
			con.disconnect();
		}
	}

	private boolean robotsTXT(URLInfo url) throws IOException {
		URL u = new URL((url.isSecure() ? "https://" : "http://") + url.getHostName() + "/robots.txt");
		HttpURLConnection con = (HttpURLConnection)u.openConnection();
		if(url.isSecure() == true)
			con = (HttpsURLConnection) con;
		con.setRequestMethod("HEAD");
		con.setRequestProperty("User-Agent", "cis455crawler");
		con.connect();
		
		if (con.getResponseCode() != 200) {
			con.disconnect();
			return false;
		}
		
		InputStream in = u.openStream();
		byte[] content = in.readAllBytes();
		String[] contentString = new String(content).split("\n");
		int position = -1;
		boolean isBlocked = false;
		
		for (int i = 0; i < contentString.length; i++) {
			if(contentString[i].toLowerCase().equals("user-agent: cis455crawler")) {
				position = i;
				break;
			}
		}
		if (position == -1) {
			for (int i = 0; i < contentString.length; i++) {
				if(contentString[i].toLowerCase().equals("user-agent: *")) {
					position = i;
					break;
				}
			}
		}
		if (position == -1)
			return false;
		while (contentString[position].length() != 0) {
			String[] rule = contentString[position].split(": ");
			if (rule[0].toLowerCase().equals("disallow")) {
				if(url.getFilePath().startsWith(rule[1])) {
					isBlocked = true;
				}
			}
			else if (rule[0].toLowerCase().equals("crawl-delay")) {
				this.crwalDelay = Integer.valueOf(rule[1]);
			}
			position++;
		}
		
		return isBlocked;
	}

	public void parseHTMLContent(URLInfo url) {
		try {
			URL u = new URL(url.toString());
			HttpURLConnection con = (HttpURLConnection)u.openConnection();
			if(url.isSecure() == true)
				con = (HttpsURLConnection) con;
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "cis455crawler");
			con.connect();
			
			/* 
			 * First we get the content
			 *  */
			InputStream inputStream = con.getInputStream();
			byte[] contentByte = inputStream.readAllBytes();
			String content = new String(contentByte);
			/* Second indexing & parsing */
            // indexing
	        if (indexing(url.toString(), content) <= 0)
	        	return;
	        
	        // For html we should parse to extract links
	        Document doc = Jsoup.parse(content);
            Elements links = doc.select("a[href]");
            for (Element link : links) {
            	URLInfo urlNew = new URLInfo(link.attr("href"));    
            	if (urlNew.getHostName() == null) {
            		if (link.attr("href").startsWith("//")) {
            			
            			String urlLegal = (url.isSecure() ? "https://" : "http://") + link.attr("href");
            			urlNew = new URLInfo(urlLegal);
            		}
            		else if (link.attr("href").startsWith("#")) {
            			urlNew.setPortNo(0);
            		}
            		else  {
            			urlNew.setHostName(url.getHostName());
                		urlNew.setPortNo(url.getPortNo());
                		urlNew.setSecure((url.getPortNo() == 443) ? true : false);
                		
                		String fp = "";
                		
                		if (url.getFilePath() == null) {
                			fp = "/" + link.attr("href");
                		}
                		else {
                			fp = (!url.getFilePath().endsWith("/") && !link.attr("href").startsWith("/")) ?
                    				url.getFilePath() + "/" + link.attr("href") :
                    					url.getFilePath() + link.attr("href");
                		}
                			
                		
                		urlNew.setFilePath(fp);
            		}
            	}
            	if(urlNew.getPortNo() == 443 || urlNew.getPortNo() == 80) {
            		this.urlQueue.enqueue(urlNew.toString());
            	}
            }
            
            inputStream.close();
            con.disconnect();
		} catch (MalformedURLException e) {
            System.out.println("The specified URL is malformed: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("An I/O error occurs: " + e.getMessage());
        }
	}

	private void parseXMLContent(URLInfo url) throws IOException {
		URL obj = new URL(url.toString());
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		InputStream inputStream = con.getInputStream();
		
		byte[] contentByte = inputStream.readAllBytes();
		String content = new String(contentByte);
		
        indexing(url.toString(), content);
        inputStream.close();
        con.disconnect();
	}
	private synchronized int indexing(String url, String content) {
		logger.info(url + ": Indexing");
		int status = db.addDocument(url, content); 
		if (status == 0) {
			logger.info(url + ": Indexing failed: Content seen");
		}
		else if (status == -1) { // indexing success
			logger.info(url + ": Indexing failed");
		}
		else {
			logger.info(url + ": Indexing success");
		}
		return status;
	}
	public void setWorking (boolean isWorking) {
		this.isWorking = isWorking;
	}
	
	public int docCollected() {
		return this.docCollected;
	}
	
	public void closeDb() {
		this.db.close();
	}
}
