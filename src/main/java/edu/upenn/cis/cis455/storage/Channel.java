package edu.upenn.cis.cis455.storage;

import java.util.LinkedList;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class Channel {
	@PrimaryKey
	private String channelName;
	
	private String channel;
	
	private LinkedList<DocumentChannel> docs;
	
	private String userName;
	
	/* For DPL binding only*/
	private Channel() {
	}
	
	public Channel(String channel, String username, String channelname) {
		this.channel = channel;
		this.userName = username;
		this.channelName = channelname;
		docs = new LinkedList<DocumentChannel>();
	}
	
	public void addDocs(DocumentChannel d) {
		this.docs.add(d);
	}
	
	public LinkedList<DocumentChannel> getDocs() { return this.docs; }
	
	public String getChannel() { return this.channel; }
	
	public String getUserName() { return this.userName; }
	
	public String getChannelName() { return this.channelName; }
	
	public int getChannelNum() { return this.channel.substring(1).split("/").length; }
	
	public String toString() {
		// "<li><a href=\"/\">Main Page</a></li> \r\n"
		String hypRef = "/show?channel=" + channelName;
		
		return "Channel name: " + this.channelName + ", created by: " + this.userName + "\r\n"
				+ "<a href=" + hypRef +">" + "link"  + "</a>" + "\r\n";
	}
}
