package edu.upenn.cis.cis455.storage;

import java.util.ArrayList;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockMode;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class ChannelDAO {

	/* The database manager from which this DAO is created. */
	private final DPLDbManager dbMgr;

	/* The document store. */
	private final EntityStore documentStore;

	/*
	 *  The primary key index.
	 *  A channel could have several documents 
	 *  */
	private final PrimaryIndex<String, Channel> idIndex;
	
	public ChannelDAO(DPLDbManager dbManager) throws DatabaseException {
		dbMgr = dbManager;

		StoreConfig documentCfg = new StoreConfig().setTransactional(true);
		documentCfg.setAllowCreate(true);
		documentStore = new EntityStore(dbMgr.env, "ChannelledDocument", documentCfg);
		
		idIndex = documentStore.getPrimaryIndex(String.class, Channel.class);
	}
	
	public void close() throws DatabaseException {
		documentStore.close();
	}
	
	public void saveChannel(Channel channel) throws DatabaseException {
		idIndex.put(dbMgr.getCurrentTxn(), channel);
	}
	
	public Channel getChannel(String channel) throws DatabaseException {
		return idIndex.get(dbMgr.getCurrentTxn(), channel, LockMode.READ_COMMITTED);
	}
	
	public ArrayList<Channel> getChannel() throws DatabaseException {
		ArrayList<Channel> allChannel = new ArrayList<Channel>();
		EntityCursor<Channel> cursor = idIndex.entities();
		
		for (Channel c : cursor) { allChannel.add(c); }
		cursor.close();
		return allChannel;
	}
	
	public void deleteChannel(String channel) throws DatabaseException {
		idIndex.delete(dbMgr.getCurrentTxn(), channel);
	}
}
