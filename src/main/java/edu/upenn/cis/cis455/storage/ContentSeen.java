package edu.upenn.cis.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import java.security.NoSuchAlgorithmException;
@Entity
public class ContentSeen {
	@PrimaryKey
	private String md5;
	
	private String content;
	
	public ContentSeen(String content) {
		this.content = content;
		this.md5 = md5Helper(this.content);
	}
	
	/* For DPL binding only*/
	private ContentSeen() {
	}
	
	public String getMd5() {
		return this.md5;
	}
	
	public static String md5Helper(String md5) {
		try {
		    java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
		    byte[] array = md.digest(md5.getBytes());
		    StringBuffer sb = new StringBuffer();
		    for (int i = 0; i < array.length; ++i) {
		    	sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
		    }
		    return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}
}
