package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockMode;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class ContentSeenDAO {
	/* The database manager from which this DAO is created. */
	private final DPLDbManager dbMgr;

	/* The document store. */
	private final EntityStore contentStore;
	
	/* The primary key index. */
	private final PrimaryIndex<String, ContentSeen> idIndex;
	
	public ContentSeenDAO(DPLDbManager dbManager) throws DatabaseException {
		dbMgr = dbManager;

		StoreConfig contentCfg = new StoreConfig().setTransactional(true);
		contentCfg.setAllowCreate(true);
		contentStore = new EntityStore(dbMgr.env, "ContentSeen", contentCfg);
		
		idIndex = contentStore.getPrimaryIndex(String.class, ContentSeen.class);
	}
	
	public void close() throws DatabaseException {
		contentStore.close();
	}
	
	public void saveContent(ContentSeen c) throws DatabaseException {
		idIndex.putNoOverwrite(dbMgr.getCurrentTxn(), c);
	}
	
	public ContentSeen getContent(ContentSeen c) throws DatabaseException {
		return idIndex.get(dbMgr.getCurrentTxn(), c.getMd5(), LockMode.READ_COMMITTED);
	}
	
	public void deleteContent(String md5) throws DatabaseException {
		idIndex.delete(dbMgr.getCurrentTxn(), md5);
	}
}
