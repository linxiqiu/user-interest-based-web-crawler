package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.Transaction;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DPLDbManager {
	/* The managed database environment handle. */
	protected final Environment env;

	/* The current open transaction. */
	private Transaction currentTxn;
	
	/**
	 * Construct a database manager for the database environment at the given
	 * home directory.
	 *
	 * @param envHome the home directory name of the database environment
	 * @throws Exception on error
	 */
	protected DPLDbManager(File envHome) throws Exception {
		// open/create a transactional environment
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);
		envConfig.setTransactional(true);
		
		if (!Files.exists(Paths.get(envHome.toURI()))) {
            try {
                Files.createDirectory(Paths.get(envHome.toURI()));
                System.out.println("yes");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
		env = new Environment(envHome, envConfig);
		currentTxn = null;
	}

	
	public void close() throws DatabaseException {
		env.close();
	}
	
	public void beginTxn() throws DatabaseException {
		currentTxn = env.beginTransaction(null, null);
	}
	
	public void commit() throws DatabaseException {
		currentTxn.commit();
	}
	
	public void abort() throws DatabaseException {
		currentTxn.abort();
		currentTxn = null;
	}

	public Transaction getCurrentTxn() {
		// TODO Auto-generated method stub
		return currentTxn;
	}
	
	public UserDAO createTicketDAO() throws DatabaseException {
		return new UserDAO(this);
	}
	
	public DocumentDAO createDocumentDAO() throws DatabaseException {
		return new DocumentDAO(this);
	}
	
	public ContentSeenDAO createContentSeenDAO() throws DatabaseException {
		return new ContentSeenDAO(this);
	}
	
	public ChannelDAO createChannelDAO() throws DatabaseException {
		return new ChannelDAO(this);
	}
}
