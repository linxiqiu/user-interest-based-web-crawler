package edu.upenn.cis.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;

@Entity
public class Document {
	@PrimaryKey
	private String url;
	
	private String documentContents;
	
	private int id;
	
	private String type;
	
	private String time;
	
	public Document(String url, String documentContents) { 
		this.url = url;
		this.documentContents = documentContents;
		
		if (url.endsWith(".xml"))
			setDocType("xml");
		else
			setDocType("html");
		
		setCrawlTime();
	}
	
	/* For DPL binding only*/
	private Document() {
	}
	
	public String getContent() { return this.documentContents; } 
	
	public String getURL() { return this.url; }
	
	public void setId(int id) { this.id = id; }
	
	public int getId() { return this.id; }
	
	public void setCrawlTime() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");  
		String now = LocalDateTime.now().format(dtf);
		this.time = now;
	}
	
	public String getCrawlTime() {
		return this.time;
	}
	
	public void setDocType(String type) {
		this.type = type;
	}
	
	public String getDocType() {
		return this.type;
	}
}
