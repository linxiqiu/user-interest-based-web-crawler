package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockMode;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class DocumentDAO {
	/* The database manager from which this DAO is created. */
	private final DPLDbManager dbMgr;

	/* The document store. */
	private final EntityStore documentStore;

	/* The primary key index. */
	private final PrimaryIndex<String, Document> idIndex;
	
	/* Document id */
	private static int id = 1;
	
	public DocumentDAO(DPLDbManager dbManager) throws DatabaseException {
		dbMgr = dbManager;

		StoreConfig documentCfg = new StoreConfig().setTransactional(true);
		documentCfg.setAllowCreate(true);
		documentStore = new EntityStore(dbMgr.env, "Document", documentCfg);
		
		idIndex = documentStore.getPrimaryIndex(String.class, Document.class);
	}
	
	public void close() throws DatabaseException {
		documentStore.close();
	}
	
	public int saveDocument(Document d) throws DatabaseException {
		idIndex.putNoOverwrite(dbMgr.getCurrentTxn(), d);
		d.setId(id++);
		return d.getId();
	}
	
	public Document getDocument(String url) throws DatabaseException {
		return idIndex.get(dbMgr.getCurrentTxn(), url, LockMode.READ_COMMITTED);
	}
	
	public void deleteDocument(String url) throws DatabaseException {
		idIndex.delete(dbMgr.getCurrentTxn(), url);
	}
	
	public int getCorpse() {
		return id;
	}
}
