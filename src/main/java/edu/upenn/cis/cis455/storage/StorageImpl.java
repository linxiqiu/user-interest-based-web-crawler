package edu.upenn.cis.cis455.storage;
import java.io.File;
import java.util.*;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class StorageImpl implements StorageInterface {
	private File dir;
	private DPLDbManager dbMgr;

	public StorageImpl(String directory) {
		dir = new File(directory);
		try {
			dbMgr = new DPLDbManager(dir);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public int getCorpusSize() {
		// TODO Auto-generated method stub
		DocumentDAO documentDAO = dbMgr.createDocumentDAO();
		int size = documentDAO.getCorpse();
		documentDAO.close();
		return size;
	}

	@Override
	public int addDocument(String url, String documentContents) {
		boolean ifSeen = getContent(documentContents);
		if (ifSeen == true)
			return 0;
		
		// this content is valid, so index it
		DocumentDAO documentDAO = dbMgr.createDocumentDAO();
		Document d = new Document(url, documentContents);
		int id = documentDAO.saveDocument(d);
		
		// store channel and corresponding document
		//addChannel(d);
		
		// save md5 at the same time 
		addContent(documentContents);
		
		documentDAO.close();
		return id;
	}
	


	@Override
	public String getDocument(String url) {
		// TODO Auto-generated method stub
		DocumentDAO documentDAO = dbMgr.createDocumentDAO();
		Document d = documentDAO.getDocument(url);
		
		if (d == null) {
			documentDAO.close();
			return null;
		}
		String content = d.getContent();
		documentDAO.close();
		return content;
	}

	@Override
	public int addUser(String username, String password) {
		// TODO Auto-generated method stub
		UserDAO uDAO = dbMgr.createTicketDAO();
		User u = new User(username, password);
		if (uDAO.getUser(username) == null) {
			int id = uDAO.saveUser(u);
			uDAO.close();
			return id;
		}
		else
			return -1;
	}

	@Override
	public boolean getSessionForUser(String username, String password) {
		// TODO Auto-generated method stub
		try {
			UserDAO uDAO = dbMgr.createTicketDAO();
			User user = uDAO.getUser(username);
			if (user == null)
				return false;
			if (user.checkPassword(password) == false) {
				uDAO.close();
				return false;
			}
			else {
				uDAO.close();
				return true;
			}
		} catch (DatabaseException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return false;
	}

	/*
	 * ContentSeen method: save md5
	 */
	public void addContent(String content) {
		ContentSeenDAO csDAO = dbMgr.createContentSeenDAO();
		ContentSeen cs = new ContentSeen(content);
		csDAO.saveContent(cs);
		csDAO.close();
	}
	
	/*
	 * get md5 to check if this content was seen before
	 */
	public boolean getContent(String content) {
		ContentSeenDAO csDAO = dbMgr.createContentSeenDAO();
		ContentSeen cs = new ContentSeen(content);
		ContentSeen csFetched = csDAO.getContent(cs);
		csDAO.close();
		
		/* Content does not exist */
		if (csFetched == null)
			return false;
		return true;
	}
	
	@Override
	public void addChannel(Channel channel) {
		ChannelDAO cDAO = dbMgr.createChannelDAO();
		cDAO.saveChannel(channel);
		cDAO.close();
	}
	
	@Override
	public Channel getChannel(String channel) {
		try {
			ChannelDAO cDAO = dbMgr.createChannelDAO();
			Channel c = cDAO.getChannel(channel);
			
			if (c == null) {
				cDAO.close();
				return null;
			}
			
			cDAO.close();
			return c;
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<Channel> getChannel() {
		ChannelDAO cDAO = dbMgr.createChannelDAO();
		ArrayList<Channel> channels = cDAO.getChannel();
		cDAO.close();
		return channels;
	}
	
	@Override
	public void close() {
		dbMgr.close();
	}
}
