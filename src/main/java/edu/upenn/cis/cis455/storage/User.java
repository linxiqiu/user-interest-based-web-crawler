package edu.upenn.cis.cis455.storage;

import java.security.MessageDigest;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class User {
	@PrimaryKey
	private String user;
	
	private String pass;
	
	private int id;
	
	public User (String user, String pass) {
		this.user = user;
		
		/* Save Encrypted password */
		this.pass = sha256(pass);
	}
	
	
	/* For DPL binding only*/
	private User() {
	}
	
	public String getUser() { return this.user; }
	
	public String getPass() { return this.pass; }
	
	public void setId(int id) { this.id = id; }
	
	public int getId() { return this.id; }


	public boolean checkPassword(String password) {
		// TODO Auto-generated method stub
		if (this.pass.equals(sha256(password)))
			return true;
		else
			return false;
	}
	
	public String sha256(final String base) {
	    try{
	        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        final byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        final StringBuilder hexString = new StringBuilder();
	        for (int i = 0; i < hash.length; i++) {
	            final String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) 
	              hexString.append('0');
	            hexString.append(hex);
	        }
	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}
}
