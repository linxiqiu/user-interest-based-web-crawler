package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockMode;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class UserDAO {
	/* The database manager from which this DAO is created. */
	private final DPLDbManager dbMgr;

	/* The user store. */
	private final EntityStore userStore;

	/* The primary key index. */
	private final PrimaryIndex<String, User> idIndex;
	
	/* The generated User ID */
	private static int idCount = 1;

	public UserDAO(DPLDbManager dbManager) throws DatabaseException {
		dbMgr = dbManager;

		StoreConfig userCfg = new StoreConfig().setTransactional(true);
		userCfg.setAllowCreate(true);
		userStore = new EntityStore(dbMgr.env, "User", userCfg);
		
		idIndex = userStore.getPrimaryIndex(String.class, User.class);
	}
	
	public void close() throws DatabaseException {
		userStore.close();
	}
	
	public int saveUser(User u) throws DatabaseException {
		idIndex.putNoOverwrite(dbMgr.getCurrentTxn(), u);
		u.setId(idCount++);
		return u.getId();
	}
	
	public User getUser(String user) throws DatabaseException {
		return idIndex.get(dbMgr.getCurrentTxn(), user, LockMode.READ_COMMITTED);
	}
	
	public void deleteUser(String user) throws DatabaseException {
		idIndex.delete(dbMgr.getCurrentTxn(), user);
	}


}
