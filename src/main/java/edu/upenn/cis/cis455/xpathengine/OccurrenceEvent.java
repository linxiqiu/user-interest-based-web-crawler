package edu.upenn.cis.cis455.xpathengine;

/**
 This class encapsulates the tokens we care about parsing in XML (or HTML)
 */
public class OccurrenceEvent {
	public enum Type {Open, Close, Text};
	
	Type type;
	
	String url;
	
	String value;
	
	public OccurrenceEvent(String type, String value) {
		this(Type.Open, value);
			
		if (type.equals("CLOSE"))
			this.setType(Type.Close);
		else if (type.equals("TEXT"))
			this.setType(Type.Text);
	}
	
	public OccurrenceEvent(Type t, String value) {
		this.type = t;
		this.value = value;
	}

	public Type getType() {
		return type;
	}
	
	public void setType(String t) {
		if (t == "TEXT") {
			setType(Type.Text);
		}
		else if (t == "CLOSE") {
			setType(Type.Close);
		}
	}
	
	public void setType(Type type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public void setURL(String url) {
		this.url = url;
	}
	
	public String getURL() {
		return this.url;
	}
	
	public String toString() {
		if (type == Type.Open) 
			return "<" + value + ">";
		else if (type == Type.Close)
			return "</" + value + ">";
		else
			return value;
	}
}
