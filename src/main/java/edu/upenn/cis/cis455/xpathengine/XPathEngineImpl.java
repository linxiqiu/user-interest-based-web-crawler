package edu.upenn.cis.cis455.xpathengine;

import java.util.*;
import java.util.regex.*;

import edu.upenn.cis.cis455.storage.Channel;

public class XPathEngineImpl implements XPathEngine {
	
	private Map<Integer, List<String>> XPaths = new HashMap<Integer, List<String>>();
	
	private Map<Integer, Integer> cursor = new HashMap<Integer, Integer>();
	
	private int queryId = 0; 
	
	private final Pattern wholeText = Pattern.compile("\\w*\\[\\w*\\(\\)\\ *\\=\\ *\\\"(.*)\\\"\\]");
	
	private final Pattern containsText = Pattern.compile("\\w*\\[\\w*\\(\\w*\\(\\)\\,\\\"(.*)\\\"\\)\\]");
	@Override
	public void setXPaths(String[] expressions) {
		// TODO Auto-generated method stub
		ArrayList<String> expList = new ArrayList<String>();
		ArrayList<Boolean> stateList = new ArrayList<Boolean>();
		
		// First we add all the registered XPath		
		for (String e : expressions) {
			expList.add(e);
		}
		
		// Second, initialize the state for this XPath
		Collections.fill(stateList, Boolean.FALSE);
		
		XPaths.put(queryId, expList);
		cursor.put(queryId, 0);
		queryId++;
	}

	@Override
	public boolean[] evaluateEvent(OccurrenceEvent event) {
		// TODO Auto-generated method stub
		boolean[] immeState = new boolean[queryId];
		// Cursor stays
		if (event.getType().equals(OccurrenceEvent.Type.Open)) {
			for (int i = 0; i < queryId; i++) {
				int curPos = cursor.get(i) < 0 ? 0 : cursor.get(i);
				
				List<String> XPath = XPaths.get(i);
				
				// First consider XPath's boundary
				if (curPos >= XPath.size()) {
					immeState[i] = false;
					continue;
				}
				
				String curXPath = XPath.get(curPos);
				int index = curXPath.indexOf("[");
				if (index > -1) {
					curXPath = curXPath.substring(0, index);
				}
				
				if (event.getValue().equals(curXPath)) {
					immeState[i] = true;
				}
				else {
					immeState[i] = false;
				}
				cursor.put(i, ++curPos);
			}
		}
		
		// Cursor move forward
		else if (event.getType().equals(OccurrenceEvent.Type.Text)) {
			for (int i = 0; i < queryId; i++) {
				
				int curPos = cursor.get(i) - 1 < 0 ? 0 : cursor.get(i) - 1;
				List<String> XPath = XPaths.get(i);
				
				if (curPos == XPath.size()) {
					immeState[i] = true;
					continue;
				}
				
				String curXPath = XPath.get(curPos);
				
				if (Pattern.matches(wholeText.toString(), curXPath)) {
					Matcher m = wholeText.matcher(curXPath);
					m.matches();
					String group = m.group(1);
					if (event.getValue().equals(group))
						immeState[i] = true;
					else
						immeState[i] = false;
					
					cursor.put(i, ++curPos);
				}
				else if (Pattern.matches(containsText.toString(), curXPath)) {

					Matcher m = containsText.matcher(curXPath);
					m.matches();
					String group = m.group(1);
					if (event.getValue().contains(group))
						immeState[i] = true;
					else
						immeState[i] = false;
					
					cursor.put(i, ++curPos);
				}
				else {
					immeState[i] = true;
				}
			}
		}
		
		// Cursor move backward
		else {
			for (int i = 0; i < queryId; i++) {
				int curPos = cursor.get(i);
				cursor.put(i, --curPos);
			}
		}
		return immeState;
	}	
	
}
