package edu.upenn.cis.cis455;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RunAllTests extends TestCase 
{
  public static Test suite() 
  {
    try {
      Class[]  testClasses = new Class[] {
        /* TODO: Add the names of your unit test classes here */
        // Class.forName("your.class.name.here") 
    		  Class.forName("edu.upenn.cis.cis455.TestExtractingHTML"),
    		  Class.forName("edu.upenn.cis.cis455.TestDuplicateData"),
    		  Class.forName("edu.upenn.cis.cis455.TestRetrieveFile"),
    		  Class.forName("edu.upenn.cis.cis455.TestSavingRetrievingXML")
      };   
      
      return new TestSuite(testClasses);
    } catch(Exception e){
      e.printStackTrace();
    } 
    
    return null;
  }
}
