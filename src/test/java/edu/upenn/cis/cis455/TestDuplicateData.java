package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import junit.framework.TestCase;

public class TestDuplicateData extends TestCase {
	@org.junit.Test
	public void testAdd() {
		/*
		 * In previous test, this was tested to be valid.
		 */
        StorageInterface db = StorageFactory.getDatabaseInstance("databaseTest");
		assertTrue(db.addUser("qiul", "123") > 0);
		assertTrue(db.addUser("qiul", "123") == -1);
	}
}
