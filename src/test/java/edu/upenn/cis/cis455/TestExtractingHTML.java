package edu.upenn.cis.cis455;

import org.junit.Test;

import edu.upenn.cis.cis455.crawler.utils.BlockingQueue;
import edu.upenn.cis.cis455.crawler.utils.CrawlerWorker;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import junit.framework.TestCase;

public class TestExtractingHTML extends TestCase {
	
	@Test
	public void testAdd() {
		BlockingQueue urlQueue = new BlockingQueue();
		
        StorageInterface db = StorageFactory.getDatabaseInstance("databaseTest");

		CrawlerWorker cw = new CrawlerWorker(db, urlQueue, 100);
		
		URLInfo url = new URLInfo("https://bitbucket.org/upenn-cis555/validation-test-files/raw/c60d3b123b53889af47e9d70edd4ad309793292a/hw2m1/index.html");
		cw.parseHTMLContent(url);
		
		try {
			String urlInTheQueue = urlQueue.dequeue();
			assertTrue(urlInTheQueue.contains("/other.html"));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
