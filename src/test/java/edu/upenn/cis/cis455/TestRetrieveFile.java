package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.crawler.utils.BlockingQueue;
import edu.upenn.cis.cis455.crawler.utils.CrawlerWorker;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import junit.framework.TestCase;

public class TestRetrieveFile extends TestCase  {

	public void testAdd() {
		BlockingQueue urlQueue = new BlockingQueue();
		
		/*
		 * In previous test, this was tested to be valid.
		 */
        StorageInterface db = StorageFactory.getDatabaseInstance("databaseTest");

		CrawlerWorker cw = new CrawlerWorker(db, urlQueue, 100);
		
		URLInfo url = new URLInfo("https://bitbucket.org/upenn-cis555/validation-test-files/raw/c60d3b123b53889af47e9d70edd4ad309793292a/hw2m1/index.html");
		cw.parseHTMLContent(url);
		
		String content = db.getDocument("https://bitbucket.org:443/upenn-cis555/validation-test-files/raw/c60d3b123b53889af47e9d70edd4ad309793292a/hw2m1/index.html");
		
		assertTrue(content.contains("A HREF=\"other.html\">Other Page"));
	}
}
