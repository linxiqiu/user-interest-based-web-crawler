package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.crawler.utils.BlockingQueue;
import edu.upenn.cis.cis455.crawler.utils.CrawlerWorker;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import junit.framework.TestCase;

public class TestSavingRetrievingXML extends TestCase {

	public void testAdd() {
		BlockingQueue urlQueue = new BlockingQueue();
		
		/*
		 * In previous test, this was tested to be valid.
		 */
        StorageInterface db = StorageFactory.getDatabaseInstance("databaseTest");

		CrawlerWorker cw = new CrawlerWorker(db, urlQueue, 100);
		
		URLInfo url = new URLInfo("http://crawltest.cis.upenn.edu/nytimes/Africa.xml");
		cw.parseHTMLContent(url);
		
		assertTrue(db.getDocument(url.toString()).contains("xml"));
		
	}
}
