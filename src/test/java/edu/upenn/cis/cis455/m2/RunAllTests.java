package edu.upenn.cis.cis455.m2;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class RunAllTests extends TestCase 
{
  public static Test suite() 
  {
    try {
      Class[]  testClasses = new Class[] {
        /* TODO: Add the names of your unit test classes here */
        // Class.forName("your.class.name.here")
    		  Class.forName("edu.upenn.cis.cis455.m2.TestHTMLXPath"),
    		  Class.forName("edu.upenn.cis.cis455.m2.TestXPathMatch"),
    		  Class.forName("edu.upenn.cis.cis455.m2.TestRegexMatch"),
    		  Class.forName("edu.upenn.cis.cis455.m2.TestTreeTraverse"),
    		  Class.forName("edu.upenn.cis.cis455.m2.TestShowReq")
      };   
      
      return new TestSuite(testClasses);
    } catch(Exception e){
      e.printStackTrace();
    } 
    
    return null;
  }
}
