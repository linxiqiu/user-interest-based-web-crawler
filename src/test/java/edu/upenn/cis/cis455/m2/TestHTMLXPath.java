package edu.upenn.cis.cis455.m2;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;

import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.DocumentChannel;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.cis455.xpathengine.XPathEngineImpl;
import junit.framework.TestCase;

public class TestHTMLXPath extends TestCase {
	static LinkedList<OccurrenceEvent> list = new LinkedList<OccurrenceEvent>(); 
	
	private static boolean[] stateUpdate;
	
    /**
     * All registered channels are here, we need to update some of them if matched.
     */
    private static ArrayList<Channel> channelList;
    
    /**
     * The state stack for each channel
     */
    private static Map<Integer, List<Boolean>> stateList = new HashMap<Integer, List<Boolean>>();
	
    private static String url = "fake";
    
	public static void traverse(Node node) {
		OccurrenceEvent occEve = new OccurrenceEvent("OPEN", node.nodeName());
		occEve.setURL(url);
		if (node.nodeName() != "#text")
			list.add(occEve);
			
		
		if (node.childNodes().size() == 0) {
			occEve = new OccurrenceEvent("TEXT", node.toString().trim());
			occEve.setURL(url);
			list.add(occEve);
		}
		
		for (Node n : node.childNodes()) {
			traverse(n);
		}
		occEve = new OccurrenceEvent("CLOSE", node.nodeName());
		occEve.setURL(url);
		if (node.nodeName() != "#text")
			list.add(occEve);
	}
	
	public void testAdd() {
		String content =  "<HTML><HEAD><TITLE>CSE455/CIS555 HW2 Sample Data</TITLE></HEAD><BODY> " + 
				 "<H2 ALIGN=center>CSE455/CIS555 HW2 Sample Data</H2> " + 
				 " " + 
				 "<P>This page contains some sample data for your second homework  " + 
				 "assignment. The HTML pages do not contain external links, so you shouldn't  " + 
				 "have to worry about your crawler &ldquo;escaping&rdquo; to the outside  " + 
				 "web. The XML files do, however, contain links to external URLs, so  " + 
				 "you'll need to make sure your crawler does not follow links in XML  " + 
				 "documents.</P> " + 
				 " " + 
				 "<H3>RSS Feeds</H3> " + 
				 "<UL> " + 
				 "<LI><A HREF=\"nytimes/\">The New York Times</A></LI> " + 
				 "<LI><A HREF=\"bbc/\">BBC News</A></LI> " + 
				 "<LI><A HREF=\"cnn/\">CNN</A></LI> " + 
				 "<LI><A HREF=\"international/\">News in foreign languages</A></LI> " + 
				 "</UL> " + 
				 " " + 
				 "<H3>Other XML data</H3> " + 
				 "<UL> " + 
				 "<LI><A HREF=\"misc/weather.xml\">Weather data</A></LI> " + 
				 "<LI><A HREF=\"misc/eurofxref-daily.xml\">Current Euro exchange rate data</A></LI> " + 
				 "<LI><A HREF=\"misc/eurofxref-hist.xml\">Historical Euro exchange rate data</A></LI> " + 
				 "</UL> " + 
				 " " + 
				 "<H3>Marie's XML data</H3> " + 
				 "<UL> " + 
				 "<LI><A HREF=\"marie/\">More data</A></LI> " + 
				 "<LI><A HREF=\"marie/private\">Private</A></LI> " + 
				 "</UL> " + 
				 " " + 
				 "<h3>Duplicates (for the content-seen test)</h3> " + 
				 "<ul> " + 
				 "<li><a href=\"misc/moreweather.xml\">More weather data</a> (an exact duplicate " + 
				 "of the <a HREF=\"misc/weather.xml\">weather data</a> from above) " + 
				 "<li><a href=\"cnn/cnn_world.rss.xml\">CNN World News</a> (a duplicate link " + 
				 "that also appears on the <a href=\"cnn/index.html\">CNN</a> page) " + 
				 "</ul> " + 
				 " " + 
				 "</BODY></HTML> ";
		Document doc = Jsoup.parse(content);
		
		Node root = doc.root();
		for (Node n : doc.childNodes()) {
			if (n.nodeName().toLowerCase().equals("rss") || n.nodeName().toLowerCase().equals("html")) {
				root = n;
			}
		}
		traverse(root);
		
		XPathEngineImpl XPathEngine = (XPathEngineImpl) XPathEngineFactory.getXPathEngine();
		int channelId = 1;
		Channel c1 = new Channel("/HTML/HEAD/TITLE", "q", "HTML");
		Channel c2 = new Channel("/html/head/title", "q", "HTML");
		
		channelList = new ArrayList<Channel>();
		channelList.add(c1);
		channelList.add(c2);
		
		for (Channel c : channelList) {
			String[] XPath = c.getChannel().substring(1).split("/");
			if (XPath[0].toLowerCase().equals("html")) {
				XPath = c.getChannel().toLowerCase().substring(1).split("/");
			}
			
			XPathEngine.setXPaths(XPath);
			stateList.put(channelId, new ArrayList<Boolean>());
			channelId++;
		}
		
		
		for (OccurrenceEvent e : list) {
			//System.out.println(e.getType() + ": " + e.getValue());
			stateUpdate = XPathEngine.evaluateEvent(e);
			int iter = 0;
			// Insert new state
			if (e.getType().equals(OccurrenceEvent.Type.Open)) {
				for (List<Boolean> sl : stateList.values()) {
					sl.add(stateUpdate[iter]);
					iter++;
				}
			}
			
			// Modify state
			else if (e.getType().equals(OccurrenceEvent.Type.Text)) {
				for (List<Boolean> sl : stateList.values()) {
					if (sl.get(sl.size() - 1) == true && stateUpdate[iter] == false) {
						sl.set(sl.size() - 1, stateUpdate[iter]);
					}
					
					// Check Populating
					Channel ch = channelList.get(iter);
					if (sl.size() == ch.getChannelNum() && !sl.contains(Boolean.FALSE)) {
						ch.addDocs(new DocumentChannel(e.getURL(), e.getValue()));
					}
					iter++;
				}
			}
			
			// Annihilate state 
			else {
				for (List<Boolean> sl : stateList.values()) {
					Channel ch = channelList.get(iter);
					
					// Update the channel
					if (sl.size() == ch.getChannelNum() && !sl.contains(Boolean.FALSE)) {
						//Crawler.db.addChannel(ch);
					}
					
					sl.remove(sl.size() - 1);
				}
			}
		}

		LinkedList<DocumentChannel> l1 = c1.getDocs();
		LinkedList<DocumentChannel> l2 = c2.getDocs();
		ArrayList<String> l1Doc = new ArrayList<String>();
		ArrayList<String> l2Doc = new ArrayList<String>();
		
		for (DocumentChannel l : l1) 
			l1Doc.add(l.getContent()); 
		
		for (DocumentChannel l : l2) 
			l2Doc.add(l.getContent());
		
		assertTrue(l1Doc.get(0).equals(l2Doc.get(0)));
	}
}
