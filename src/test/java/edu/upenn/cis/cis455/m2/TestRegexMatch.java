package edu.upenn.cis.cis455.m2;

import static org.junit.Assert.assertTrue;

import java.util.regex.*;

import junit.framework.TestCase;

public class TestRegexMatch extends TestCase {

	public void testAdd() {
		Pattern wholeText = Pattern.compile("\\w*\\[\\w*\\(\\)\\ *\\=\\ *\\\"(.*)\\\"\\]");
		
		Pattern containsText = Pattern.compile("\\w*\\[\\w*\\(\\w*\\(\\)\\,\\\"(.*)\\\"\\)\\]");
		
		String s1 = "employees[text()=\" sfw\"]";
		
		String s2 = "title[contains(text(),\"U.N.\")]";
		
		Matcher m = wholeText.matcher(s1);
		
		Matcher m2 = containsText.matcher(s2);
		
		m.matches();
		
		m2.matches();
		
		assertTrue(m.group(1).equals(" sfw"));
		assertTrue(m2.group(1).equals("U.N."));
	}
}
