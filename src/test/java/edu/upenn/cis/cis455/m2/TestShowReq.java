package edu.upenn.cis.cis455.m2;

import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.DocumentChannel;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import junit.framework.TestCase;

public class TestShowReq extends TestCase {
	
	public void testAdd() {
		StorageInterface db = StorageFactory.getDatabaseInstance("databaseTest");
		
		String channelName = "/rss/channel/item/author[text()=\"MARC LACEY\"]";
    	Channel channel = db.getChannel(channelName);
    	
    	StringBuffer s = new StringBuffer();
    	for (DocumentChannel dc : channel.getDocs()) {
    		String content = "";
    		content += "Crawled on: " + dc.getCrawlTime() + "\r\n";
    		content += "Location: " + dc.getURL() + "\r\n";
    		content += "<div class=”document”>\r\n";
    		content += dc.getContent() + "\r\n";
    		content += "</div>\r\n";
    		s.append(content);
    	}
    	
    	System.out.println(s.toString());
	}
}
