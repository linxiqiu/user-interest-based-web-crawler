package edu.upenn.cis.cis455.m2;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import junit.framework.TestCase;

public class TestTreeTraverse extends TestCase {
	
	private static List<OccurrenceEvent> list = new ArrayList<OccurrenceEvent>();
	
	/**
	 * The traverse method in parser bolt that generates occurrence events
	 * @param node
	 */
	public void traverse(Node node) {
		OccurrenceEvent occEve = new OccurrenceEvent("OPEN", node.nodeName());
		if (node.nodeName() != "#text")
			list.add(occEve);
			
		
		if (node.childNodes().size() == 0) {
			occEve = new OccurrenceEvent("TEXT", node.toString().trim());
			list.add(occEve);
		}
		
		for (Node n : node.childNodes()) {
			traverse(n);
		}
		occEve = new OccurrenceEvent("CLOSE", node.nodeName());
		if (node.nodeName() != "#text")
			list.add(occEve);
	}
	public void testAdd() {
		String content = "<employees>\n"
				+ "    <employee id=\"111\">\n"
				+ "        <firstName>Lokesh</firstName>\n"
				+ "        <lastName>Gupta</lastName>\n"
				+ "        <location>India</location>\n"
				+ "    </employee>\n"
				+ "    <employee id=\"222\">\n"
				+ "        <firstName>Alex</firstName>\n"
				+ "        <lastName>Gussin</lastName>\n"
				+ "        <location>Russia</location>\n"
				+ "    </employee>\n"
				+ "    <employee id=\"333\">\n"
				+ "        <firstName>David</firstName>\n"
				+ "        <lastName>Feezor</lastName>\n"
				+ "        <location>USA</location>\n"
				+ "    </employee>\n"
				+ "</employees>";
		Document doc = Jsoup.parse(content, "", Parser.xmlParser());
		traverse(doc.root().childNode(0));
		
		int stack = 0;
		
		for (OccurrenceEvent oc : list) {
			if (oc.getType().equals(OccurrenceEvent.Type.Open))
				stack++;
			else if(oc.getType().equals(OccurrenceEvent.Type.Close))
				stack--;
		}
		assertTrue(stack == 0);
	}
}
