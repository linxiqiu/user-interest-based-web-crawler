package edu.upenn.cis.cis455.m2;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.DocumentChannel;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.cis455.xpathengine.XPathEngineImpl;
import junit.framework.TestCase;

public class TestXPathMatch extends TestCase {
	static LinkedList<OccurrenceEvent> list = new LinkedList<OccurrenceEvent>(); 
	
	private static boolean[] stateUpdate;
	
    /**
     * All registered channels are here, we need to update some of them if matched.
     */
    private static ArrayList<Channel> channelList;
    
    /**
     * The state stack for each channel
     */
    private static Map<Integer, List<Boolean>> stateList = new HashMap<Integer, List<Boolean>>();
	
    private static String url = "fake";
    
    private static StorageInterface db = StorageFactory.getDatabaseInstance("databaseTest");
    
	public static void traverse(Node node) {
		OccurrenceEvent occEve = new OccurrenceEvent("OPEN", node.nodeName());
		occEve.setURL(url);
		if (node.nodeName() != "#text")
			list.add(occEve);
			
		
		if (node.childNodes().size() == 0) {
			occEve = new OccurrenceEvent("TEXT", node.toString().trim());
			occEve.setURL(url);
			list.add(occEve);
		}
		
		for (Node n : node.childNodes()) {
			traverse(n);
		}
		occEve = new OccurrenceEvent("CLOSE", node.nodeName());
		occEve.setURL(url);
		if (node.nodeName() != "#text")
			list.add(occEve);
	}
	
	public void testAdd() {
		String content =  "<rss version=\"2.0\"> " + 
				 "<channel> " + 
				 "<title>NYT > Africa</title> " + 
				 "<link>http://www.nytimes.com/pages/international/africa/index.html?partner=rssnyt</link> " + 
				 "<description>Find breaking news, world news, multimedia and opinion on Africa from South Africa, Egypt, Ethiopia, Libya, Rwanda, Kenya, Morocco, Zimbabwe, Sudan and Algeria. </description> " + 
				 "<copyright>Copyright 2006 The New York Times Company</copyright> " + 
				 "<language>en-us</language> " + 
				 "<lastBuildDate>Thu, 2 Feb 2006 23:05:01 EST</lastBuildDate> " + 
				 "<image> " + 
				 "<url>http://graphics.nytimes.com/images/section/NytSectionHeader.gif</url> " + 
				 "<title>NYT > Africa</title> " + 
				 "<link>http://www.nytimes.com/pages/international/africa/index.html</link> " + 
				 "</image> " + 
				 "<item> " + 
				 "<title>Egypt Insists That Hamas Stop Violence</title> " + 
				 "<link>http://www.nytimes.com/2006/02/02/international/middleeast/02egypt.html?ex=1296536400&en=06debaf57bef24dc&ei=5088&partner=rssnyt&emc=rss</link> " + 
				 "<description>Egypt insisted that Hamas confirm existing agreements between Israel and the Palestinians and recognize Israel's legitimacy.</description> " + 
				 "<author>STEVEN ERLANGER</author> " + 
				 "<pubDate>Thu, 02 Feb 2006 00:00:00 EDT</pubDate> " + 
				 "<guid isPermaLink=\"false\">http://www.nytimes.com/2006/02/02/international/middleeast/02egypt.html</guid> " + 
				 "</item> " + 
				 "<item> " + 
				 "<title>World Briefings: Africa, Asia, Middle East, United Nations, Europe</title> " + 
				 "<link>http://www.nytimes.com/2006/02/03/international/03briefs.html?ex=1296622800&en=17c287b37f35e7d9&ei=5088&partner=rssnyt&emc=rss</link> " + 
				 "<description>AFRICA.</description> " + 
				 "<author>(AP)</author> " + 
				 "<pubDate>Fri, 03 Feb 2006 00:00:00 EDT</pubDate> " + 
				 "<guid isPermaLink=\"false\">http://www.nytimes.com/2006/02/03/international/03briefs.html</guid> " + 
				 "</item> " + 
				 "<item> " + 
				 "<TITLE>World Briefing: Africa, Americas, Europe and Asia</TITLE> " + 
				 "<link>http://www.nytimes.com/2006/02/02/international/02briefs.html?ex=1296536400&en=cd101688c565f27f&ei=5088&partner=rssnyt&emc=rss</link> " + 
				 "<description>AFRICA.</description> " + 
				 "<pubDate>Thu, 02 Feb 2006 00:00:00 EDT</pubDate> " + 
				 "<guid isPermaLink=\"false\">http://www.nytimes.com/2006/02/02/international/02briefs.html</guid> " + 
				 "</item> " + 
				 "<item> " + 
				 "<title>Loan for Foreign Mining in Ghana Approved</title> " + 
				 "<link>http://www.nytimes.com/2006/02/01/international/africa/01africa.html?ex=1296450000&en=6ef2e47c65509682&ei=5088&partner=rssnyt&emc=rss</link> " + 
				 "<description>The World Bank's investment agency said the loan was approved on the condition that Newmont Mining meet stringent social and environmental standards.</description> " + 
				 "<author>CELIA W. DUGGER</author> " + 
				 "<pubDate>Wed, 01 Feb 2006 00:00:00 EDT</pubDate> " + 
				 "<guid isPermaLink=\"false\">http://www.nytimes.com/2006/02/01/international/africa/01africa.html</guid> " + 
				 "</item> " + 
				 "<item> " + 
				 "<title>World Briefing: Asia, Middle East, Americas, Europe and Africa</title> " + 
				 "<link>http://www.nytimes.com/2006/02/01/international/01briefs.html?ex=1296450000&en=a744dabed3339202&ei=5088&partner=rssnyt&emc=rss</link> " + 
				 "<description>ASIA.</description> " + 
				 "<author>SALMAN MASOOD (NYT)</author> " + 
				 "<pubDate>Wed, 01 Feb 2006 00:00:00 EDT</pubDate> " + 
				 "<guid isPermaLink=\"false\">http://www.nytimes.com/2006/02/01/international/01briefs.html</guid> " + 
				 "</item> " + 
				 "<item> " + 
				 "<title>Khartoum Journal: Sudan Leader Waits, and Waits, for His Ship to Come In</title> " + 
				 "<link>http://www.nytimes.com/2006/01/31/international/africa/31khartoum.html?ex=1296363600&en=c514139910f2888c&ei=5088&partner=rssnyt&emc=rss</link> " + 
				 "<description>For a war-torn, impoverished country, a gigantic, luxurious presidential yacht.</description> " + 
				 "<author>MARC LACEY</author> " + 
				 "<pubDate>Tue, 31 Jan 2006 00:00:00 EDT</pubDate> " + 
				 "<guid isPermaLink=\"false\">http://www.nytimes.com/2006/01/31/international/africa/31khartoum.html</guid> " + 
				 "</item> " + 
				 "</channel> " + 
				 "</rss> ";
		Document doc = Jsoup.parse(content, "", Parser.xmlParser());
		traverse(doc.root().childNode(0));
		XPathEngineImpl XPathEngine = (XPathEngineImpl) XPathEngineFactory.getXPathEngine();
		int channelId = 1;
		Channel c1 = new Channel("/rss/channel/item/title", "q", "title");
		Channel c2 = new Channel("/rss/channel/item/author[text()=\"MARC LACEY\"]", "q", "author");
		
		channelList = new ArrayList<Channel>();
		channelList.add(c1);
		channelList.add(c2);
		
		for (Channel c : channelList) {
			String[] XPath = c.getChannel().substring(1).split("/");
			
			XPathEngine.setXPaths(XPath);
			stateList.put(channelId, new ArrayList<Boolean>());
			channelId++;
		}
		
		
		for (OccurrenceEvent e : list) {
			System.out.println(e.getType() + ": " + e.getValue());
			stateUpdate = XPathEngine.evaluateEvent(e);
			int iter = 0;
			// Insert new state
			if (e.getType().equals(OccurrenceEvent.Type.Open)) {
				for (List<Boolean> sl : stateList.values()) {
					sl.add(stateUpdate[iter]);
					iter++;
				}
			}
			
			// Modify state
			else if (e.getType().equals(OccurrenceEvent.Type.Text)) {
				for (List<Boolean> sl : stateList.values()) {
					if (sl.get(sl.size() - 1) == true && stateUpdate[iter] == false) {
						sl.set(sl.size() - 1, stateUpdate[iter]);
					}
					
					// Check Populating
					Channel ch = channelList.get(iter);
					if (sl.size() == ch.getChannelNum() && !sl.contains(Boolean.FALSE)) {
						ch.addDocs(new DocumentChannel(e.getURL(), e.getValue()));
					}
					iter++;
				}
			}
			
			// Annihilate state 
			else {
				for (List<Boolean> sl : stateList.values()) {
					Channel ch = channelList.get(iter);
					
					// Update the channel
					if (sl.size() == ch.getChannelNum() && !sl.contains(Boolean.FALSE)) {
						db.addChannel(ch);
					}
					
					sl.remove(sl.size() - 1);
					iter++;
				}
			}
		}

		LinkedList<DocumentChannel> l1 = c1.getDocs();
		LinkedList<DocumentChannel> l2 = c2.getDocs();
		ArrayList<String> l1Doc = new ArrayList<String>();
		ArrayList<String> l2Doc = new ArrayList<String>();
		
		for (DocumentChannel l : l1) {
			l1Doc.add(l.getContent());
			System.out.println(l.getContent());
		}
			 
		
		for (DocumentChannel l : l2) 
			l2Doc.add(l.getContent());
		
		
		assertTrue(!l1Doc.contains("World Briefing: Africa, Americas, Europe and Asia"));
		assertTrue(l2Doc.get(0).equals("MARC LACEY"));
	}
}
